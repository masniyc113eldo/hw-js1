const name = "Denis";
const admin = name;
console.log(admin)

let numDays = parseInt(prompt("Введите количество дней (от 1 до 10):"));
if (numDays < 1 || numDays > 10) {
    alert("Введенное число не находится в диапазоне от 1 до 10.");
} else {
    let numSeconds = numDays * 24 * 60 * 60;
    alert(`${numDays} дней содержит ${numSeconds} секунд.`);
}
